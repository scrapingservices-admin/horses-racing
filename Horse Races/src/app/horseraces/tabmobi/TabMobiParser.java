/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package app.horseraces.tabmobi;

import app.horseraces.core.HorseData;
import app.horseraces.core.HtmlReader;
import com.gargoylesoftware.htmlunit.html.DomNode;
import com.gargoylesoftware.htmlunit.html.DomNodeList;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author DELL
 */
public class TabMobiParser {

    private String MAIN_URL = "http://tab.mobi/tabmobi/vic/racing/todays-racing";
    private String FUTURE_RACES = "http://tab.mobi/tabmobi/vic/sports/fixed-odds-racing";
    private String DATE_REGEX = "[0-9]{2} [a-zA-Z]+ [0-9]{4}";
    private boolean debug = false;
    HtmlReader reader = new HtmlReader();
    //----------------------------------------------------------------------------------------------- l

    /**
     * doParse method Read the Main Url and retrieve it's content
     */
    public void doParse() {

        reader.configure(false);
        try {
            //todays races reader
            HtmlPage p = reader.readPage(MAIN_URL, 0);
            if (debug) {
                System.out.println(p.asText());
            }
            List<HorseData> data = parseToday(p);


            //future races reader
            HtmlPage p1 = reader.readPage(FUTURE_RACES, 0);
            if (debug) {
                System.out.println(p1.asText());
            }
            //List<HorseData> data = parseTomorrow(p1);
        } catch (IOException ex) {
            Logger.getLogger(TabMobiParser.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    //-----------------------------------------------------------------------------------------------------
    public void setDebug(boolean debug) {
        this.debug = debug;
    }

    private List<HorseData> parseToday(HtmlPage p) {
        List<HorseData> hd = new ArrayList<HorseData>();
        HorseData d = new HorseData();
        DomNodeList<DomNode> a = p.querySelectorAll("div[id=racingLinkContent] a");

        for (int i = 0; i < a.size(); i++) {
            String venueURL = a.get(i).getAttributes().getNamedItem("href").getNodeValue();
            DomNode dn = a.get(i).querySelector("div[class=todayRacing] div[class=jackpotSecndCol] span");
            //System.out.println(dn.asText());
            String line[] = dn.asText().split(" \\(");

            d.racingVenues = line[0];
            d.raceState = line[1].split("\\)")[0];

            //Going to 4th page
            try {
                HtmlPage p2 = reader.readPage("http://tab.mobi" + venueURL, 0);
                parseVenue(p2, d);
            } catch (IOException ex) {
                Logger.getLogger(TabMobiParser.class.getName()).log(Level.SEVERE, null, ex);
            }

            System.out.println(d.racingVenues + " | " + d.raceState + " | " + d.raceDate + " | " + d.raceDay);
            System.out.println("------------------------------------------------------------------\n");            
            hd.add(d);
        }

        return hd;
    }

    private void parseVenue(HtmlPage p2, HorseData d) {
        DomNode date = p2.querySelector("div[class=meetingName]");
        try {
            d.raceDate = regex(DATE_REGEX, date.asText());
        } catch (Exception ex) {
            Logger.getLogger(TabMobiParser.class.getName()).log(Level.SEVERE, null, ex);
        }
        d.raceDay = date.asText().split(" ")[0];

        //getting the races
        DomNodeList<DomNode> races = p2.querySelectorAll("div[class='completedRaces h65']");
        System.out.println("Number of Races: " + races.size());
        for (int i = 0; i < races.size(); i++) {
            DomNode link = races.get(i).querySelector("a");
            DomNode odds = races.get(i).querySelector("div[class=raceMeeting]");
            boolean hasFixedOdds = false;
            try {
                DomNodeList<DomNode> imgs = odds.querySelectorAll("img");
                
                for (int j = 0; j < imgs.size(); j++) {
                    if (imgs.get(j).getAttributes().getNamedItem("alt").getNodeValue().contains("Fixed")) {
                        hasFixedOdds = true;
                    }
                }
            } catch (Exception e) {
            }
            try {
                String number = link.querySelector("span[class=secondRow]").asText().trim();
                String time = link.querySelector("span[class=firstRow]").asText().trim();
                String linkClass = link.getAttributes().getNamedItem("class").getNodeValue();

                System.out.println("***" + linkClass + " " + time + " " + number);

                if (linkClass.contains("green")) {
                    if (hasFixedOdds) {
                        try {
                            HtmlPage p3 = reader.readPage("http://tab.mobi" + link.getAttributes().getNamedItem("href").getNodeValue()+"/FO", 0);
                            parseRace(p3, d);
                        } catch (IOException ex) {
                            Logger.getLogger(TabMobiParser.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    else{
                        System.out.println("No Fixed Odds");
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private void parseRace(HtmlPage p3, HorseData d) {
        DomNode betTab = p3.querySelector("div[class=betType] div[class=betTab]");
        DomNodeList<DomNode> rows = betTab.querySelectorAll("div[class=tableRow]");
        String conditions = "";
        conditions = p3.querySelector("div[class=tableRunrWinPlace] a[name=tdyTrackModal]").asText().split(" ")[0];
        System.out.println("Race conditions: " + conditions);

        System.out.println(rows.size());
        for (int i = 0; i < rows.size(); i++) {
            String nro = "-", horse = "-", win = "-", place = "-";
            nro = rows.get(i).querySelector("div[class=tableRowNo]").asText().replace(".", "");
            horse = rows.get(i).querySelector("div[class=tableRowRunr]").asText();
            win = rows.get(i).querySelector("div[class=tableRowWin]").asText();
            try {

                place = rows.get(i).querySelector("div[class=tableRowPlace]").asText();
            } catch (Exception e) {
                place = win;
            }
            System.out.println("******" + nro + " " + horse + " " + win + " " + place);

            // System.out.println(rows.get(i).asText());
        }
        System.out.println("");

    }

    private List<HorseData> parseTomorrow(HtmlPage p) {
        List<HorseData> hd = new ArrayList<HorseData>();
        HorseData d = new HorseData();
        DomNodeList<DomNode> li = p.querySelectorAll("div[class=contextualLinks] ul li");

        for (int i = 1; i < li.size(); i++) {
            try {
                if (li.get(i).getAttributes().getNamedItem("class").getNodeValue().contains("hdr")) {
                    break;
                }
            } catch (Exception e) {
            }
            System.out.println(li.get(i).asText());
            try {
                HtmlPage p2 = reader.readPage("http://tab.mobi" + li.get(i).querySelector("a").getAttributes().getNamedItem("href").getNodeValue(), 0);
                parseTomorrowVenues(p2, d);
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }

        return hd;
    }

    private void parseTomorrowVenues(HtmlPage p, HorseData d) {
        //System.out.println("ENTRA");
        //System.out.println(p.asXml());
        DomNodeList<DomNode> races = p.querySelectorAll("div[class='completedRaces pl25'] a");

        for (int i = 0; i < races.size(); i++) {
            System.out.println("Venue: "+races.get(i).asText().trim());
            try {
                HtmlPage p2 = reader.readPage("http://tab.mobi" + races.get(i).getAttributes().getNamedItem("href").getNodeValue(), 0);
                parseTomorrowRaces(p2, d);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            System.out.println("----------------------------------------------");
        }
    }

    private void parseTomorrowRaces(HtmlPage p2, HorseData d) {
        DomNodeList<DomNode> rows = p2.querySelectorAll("div[class=tableRow]");
        String fulldate = p2.querySelector("div[class=sportsDate1]").asText();
        String day = fulldate.split(" ")[0];
        String date = fulldate.split(" ")[1]+" "+fulldate.split(" ")[2]+" "+fulldate.split(" ")[3];

        System.out.println("Day: "+day);
        System.out.println("Date: "+date);
        
        System.out.println("Horses: "+rows.size());
        for (int i = 0; i < rows.size(); i++) {
            String horse = "-", win = "-", place = "-";
            horse = rows.get(i).querySelector("div[class=sportsTableRowRunr]").asText();
            win = rows.get(i).querySelectorAll("div[class=sportsTableRowWin]").get(0).asText();
            try {
                place = rows.get(i).querySelectorAll("div[class=sportsTableRowWin]").get(1).asText();
            } catch (Exception e) {
                place = "-";
            }
            System.out.println("******" + horse + " " + win + " " + place);

            // System.out.println(rows.get(i).asText());
        }
        System.out.println("");
    }

    public String regex(String compile, String message) throws Exception {
        Pattern p = Pattern.compile(compile);
        Matcher m = p.matcher(message);

        if (m.find()) {
            message = m.group();
        } else {
            message = "FAKE";
        }

        return message;
    }
}
