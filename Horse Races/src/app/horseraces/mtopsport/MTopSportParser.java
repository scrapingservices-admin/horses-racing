/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package app.horseraces.mtopsport;

import app.horseraces.core.HtmlReader;
import app.horseraces.mbet365.MBet365Parser;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author DELL
 */
public class MTopSportParser {
     private String MAIN_URL="http://m.topsport.com.au/#RaceCard/today";
    private String TOMORROW_URL="http://m.topsport.com.au/#RaceCard/tomorrow";
    
    private boolean debug=false;
    
   //----------------------------------------------------------------------------------------------- l
    /**
     * doParse method Read the Main Url and retrieve it's content
     */
    public void doParse(){
     HtmlReader reader=new HtmlReader();
     reader.configure(false);
        try {
            //page reader
            HtmlPage p=reader.readPage(MAIN_URL, 0);
            if(debug)
            System.out.println(p.asText());
            
       
        } catch (IOException ex) {
            Logger.getLogger(MTopSportParser.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    //-----------------------------------------------------------------------------------------------------
    public void setDebug(boolean debug) {
        this.debug = debug;
    }
}
