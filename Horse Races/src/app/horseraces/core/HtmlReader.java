/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package app.horseraces.core;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import java.io.IOException;
import java.util.logging.Level;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author DELL
 */
public class HtmlReader {
    
    public WebClient web = new WebClient(BrowserVersion.FIREFOX_24);
    
     //----------------------------------------------------------------  
     public void configure(boolean js){
     LogFactory.getFactory().setAttribute("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.NoOpLog");

        java.util.logging.Logger.getLogger("com.gargoylesoftware.htmlunit").setLevel(Level.OFF);
        java.util.logging.Logger.getLogger("org.apache.commons.httpclient").setLevel(Level.OFF);
        web.getOptions().setCssEnabled(false);
        web.getOptions().setJavaScriptEnabled(js);
        web.getOptions().setThrowExceptionOnFailingStatusCode(false);
        web.getOptions().setThrowExceptionOnScriptError(false);
        web.getOptions().setUseInsecureSSL(true);
        web.getOptions().setRedirectEnabled(true);
     
     }
   //-------------------------------------------------------------------
     public HtmlPage readPage(String page, int waitFor) throws IOException{
     
         HtmlPage p=web.getPage(page);
         web.waitForBackgroundJavaScript(waitFor);
         return p;
         
     }
     
    
}
