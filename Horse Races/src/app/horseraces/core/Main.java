/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package app.horseraces.core;

import app.horseraces.betezy.BetezyParser;
import app.horseraces.ladbrokes.LadBrokesParser;
import app.horseraces.mbet365.MBet365Parser;
import app.horseraces.sportingbet.SportingBetParser;
import app.horseraces.tabmobi.TabMobiParser;
import app.horseraces.tattsbet.TattsbetParser;

/**
 *
 * @author DELL
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        TabMobiParser tmp=new TabMobiParser();
        //tmp.setDebug(true);
        tmp.doParse();
        
        MBet365Parser mbet365=new MBet365Parser();
        mbet365.setDebug(true);
       // mbet365.doParse();
        
        LadBrokesParser lbp=new LadBrokesParser();
        //lbp.doParse();
        
        BetezyParser bp = new BetezyParser();
        //bp.doParse();
        
        SportingBetParser sbp = new SportingBetParser();
        //sbp.doParse();
        
        TattsbetParser tp = new TattsbetParser();
        //tp.doParse();
    }
}
