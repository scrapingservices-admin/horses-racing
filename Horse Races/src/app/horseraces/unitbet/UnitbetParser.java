/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package app.horseraces.unitbet;

import app.horseraces.core.HtmlReader;
import app.horseraces.mbet365.MBet365Parser;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author DELL
 * This class needs to perform click to get today/tomorrow races table and a specific
 */

public class UnitbetParser {
     private String MAIN_URL="https://rs.unibet.com.au/UNIBET/app?currency=AUD&locale=en_AU&playerId=&ticket=&elementToRenderIn=race-launcher-main-container&callback=cms.widget.AustralianRacing.clientNotification&env=beta&provider=UNIBET&is_iframe=1&_parent_href=https%253A%252F%252Fwww.unibet.com.au&_cb=1409518705609&a=1&b=0&m=0#racing";
    
    
    private boolean debug=false;
    
   //----------------------------------------------------------------------------------------------- l
    /**
     * doParse method Read the Main Url and retrieve it's content
     */
    public void doParse(){
     HtmlReader reader=new HtmlReader();
     reader.configure(false);
        try {
            //page reader
            HtmlPage p=reader.readPage(MAIN_URL, 0);
            if(debug)
            System.out.println(p.asText());
            
       
        } catch (IOException ex) {
            Logger.getLogger(UnitbetParser.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    //-----------------------------------------------------------------------------------------------------
    public void setDebug(boolean debug) {
        this.debug = debug;
    }
}
