/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package app.horseraces.sportingbet;

import app.horseraces.betezy.BetezyParser;
import app.horseraces.core.HtmlReader;
import app.horseraces.mbet365.MBet365Parser;
import com.gargoylesoftware.htmlunit.html.DomNode;
import com.gargoylesoftware.htmlunit.html.DomNodeList;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author DELL
 */
public class SportingBetParser {

    private String MAIN_URL = "https://www.sportingbet.com.au/horse-racinggrid/";
    private String TOMORROW_URL = "https://www.sportingbet.com.au/horse-racinggrid/meetings/Tomorrow";
    private String DATE_REGEX = "[0-9]{2} [a-zA-Z]+ [0-9]{4}";
    private boolean debug = false;
    HtmlReader reader = new HtmlReader();
    //----------------------------------------------------------------------------------------------- l

    /**
     * doParse method Read the Main Url and retrieve it's content
     */
    public void doParse() {

        reader.configure(false);
        try {
            //page reader
            HtmlPage p = reader.readPage(MAIN_URL, 0);
            HtmlPage p2 = reader.readPage(TOMORROW_URL, 0);
            if (debug) {
                System.out.println(p.asText());
            }
            parseToday(p);
        } catch (IOException ex) {
            Logger.getLogger(SportingBetParser.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    //-----------------------------------------------------------------------------------------------------
    public void setDebug(boolean debug) {
        this.debug = debug;
    }

    private void parseToday(HtmlPage p) {
        DomNodeList<DomNode> tr = p.querySelectorAll("tbody").get(1).querySelectorAll("tr");

        for (int i = 0; i < tr.size(); i++) {
            DomNodeList<DomNode> td = tr.get(i).querySelectorAll("td");
            System.out.println("Racing Venue: " + td.get(0).asText());
            for (int j = 1; j < td.size(); j++) {

                if (td.get(j).hasAttributes()) {
                    DomNode a = td.get(j).querySelector("a");
                    String linkClass = a.getAttributes().getNamedItem("class").getNodeValue();
                    System.out.println("-- " + linkClass);
                    String timetag = a.querySelector("time").getAttributes().getNamedItem("datetime").getNodeValue();
                    String raceday = timetag.substring(0, timetag.indexOf(","));
                    String racedate = "-";
                    try {
                        racedate = regex(DATE_REGEX, timetag);
                    } catch (Exception ex) {
                        Logger.getLogger(SportingBetParser.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    System.out.println("Race day: " + raceday + " Race Date: " + racedate);
                    String link = a.getAttributes().getNamedItem("href").getNodeValue();
                    String racenumber = link.substring(link.lastIndexOf("-") + 1);
                    System.out.println("Race number: " + racenumber);

                    HtmlPage p2;
                    try {
                        p2 = reader.readPage("https://www.sportingbet.com.au" + link, 0);
                        if (linkClass.equals("closed")) {
                            System.out.println("Closed Race");
                            parseClosedRace(p2);
                        } else {

                            parseRace(p2);
                        }
                    } catch (IOException ex) {
                        Logger.getLogger(BetezyParser.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    System.out.println("");
                }
            }
            System.out.println("-------------------------------------------------------------------");
        }
    }

    private void parseRace(HtmlPage p2) {
        DomNode t = p2.querySelector("h3[class=is-open] a[class=f-left]");
        String time = t.asText().split(" ")[0];
        System.out.println("Race time: " + time);
        DomNodeList<DomNode> rows = p2.querySelectorAll("div[class='row ']");
        DomNodeList<DomNode> sc_rows = p2.querySelectorAll("div[class='row scratched']");
        String number = "", name = "", fixedwin = "", fixedplace = "", conditions = "-", barrier = "", weather = "-";
        try {
            conditions = p2.querySelector("div[class=race-conditions] span").asText().split(" ")[0];
            weather = p2.querySelector("div[class='row race-details'] div[class='large-12 columns']").asText().split("\n")[2];
        } catch (Exception e) {
        }
        System.out.println("Race Conditions: " + conditions + " weather: " + weather);

        System.out.println("Horses:");
        for (int i = 0; i < rows.size(); i++) {
            try {
                number = rows.get(i).querySelector("div[class='large-1 small-1 columns number'] b").asText();
                barrier = rows.get(i).querySelector("div[class='large-1 small-1 columns number'] text").asText().split("\\)")[0].substring(1);
                name = rows.get(i).querySelector("div[class='large-4 small-5 columns runner'] strong").asText();
                // System.out.println("...."+rows.get(i).querySelector("div[class='large-7 small-6 columns bet-type']").asXml());
                fixedwin = rows.get(i).querySelector("div[class='large-7 small-6 columns bet-type'] li[class='fixed-win tote']").asText();
                fixedplace = rows.get(i).querySelector("div[class='large-7 small-6 columns bet-type'] li[class='fixed-place tote']").asText();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            System.out.println("***" + number + " " + barrier + " " + name + " " + fixedwin + " " + fixedplace);
        }
        System.out.println("Scratched Horses:");
        for (int i = 0; i < sc_rows.size(); i++) {
            try {
                number = sc_rows.get(i).querySelector("div[class='large-1 small-1 columns number'] b").asText();
                barrier = rows.get(i).querySelector("div[class='large-1 small-1 columns number'] text").asText().split("\\)")[0].substring(1);
                name = sc_rows.get(i).querySelector("div[class='large-4 small-5 columns runner'] strong").asText();
                // System.out.println("...."+rows.get(i).querySelector("div[class='large-7 small-6 columns bet-type']").asXml());
                fixedwin = sc_rows.get(i).querySelector("div[class='large-7 small-6 columns bet-type'] li[class='fixed-win tote']").asText();
                fixedplace = sc_rows.get(i).querySelector("div[class='large-7 small-6 columns bet-type'] li[class='fixed-place tote']").asText();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            System.out.println("***" + number + " " + barrier + " " + name + " " + fixedwin + " " + fixedplace);
        }
    }

    public String regex(String compile, String message) throws Exception {
        Pattern p = Pattern.compile(compile);
        Matcher m = p.matcher(message);

        if (m.find()) {
            message = m.group();
        } else {
            message = "FAKE";
        }

        return message;
    }

    private void parseClosedRace(HtmlPage p2) {
        DomNode t = p2.querySelector("h3[class=is-open] a[class=f-left]");
        String time = t.asText().split(" ")[0];
        System.out.println("Race time: " + time);
        DomNodeList<DomNode> rows = p2.querySelectorAll("table[class='tbl tbl-1-4'] tr");
        String number = "", name = "", win = "", place = "", conditions = "-", weather = "-";
        try {
            conditions = p2.querySelector("div[class=race-conditions] span").asText().split(" ")[0];
            weather = p2.querySelector("div[class='row race-details'] div[class='large-12 columns']").asText().split("\n")[2];
        } catch (Exception e) {
        }
        System.out.println("Race Conditions: " + conditions + " weather: " + weather);
        System.out.println(rows.size());
        for (int i = 0; i < rows.size(); i++) {
            try{
            place = rows.get(i).querySelector("td strong").asText().replace(".", "");
            String h = rows.get(i).querySelector("td").asText();
            String h2 = h.substring(h.indexOf(" ")+1,h.indexOf("\n")-1);            
            number = h2.split(" ")[0];
            name = h2.split(" ")[1];
            System.out.println(place+" "+number+" "+name);
//            System.out.println(rows.get(i).asXml());
            }catch(Exception e){}
        }
        
    }
}
