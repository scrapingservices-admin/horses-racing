/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package app.horseraces.mbet365;

import app.horseraces.core.HtmlReader;

import com.gargoylesoftware.htmlunit.html.HtmlPage;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author DELL
 * This class will Parse the Bet365 site
 * Remember, this will capture todays, and days races.
 */
public class MBet365Parser {
    
     private String MAIN_URL="https://mobile.bet365.com/sport/splash/Default.aspx?Sport=2&key=2&L=1";
    
    
    private boolean debug=false;
    
   //----------------------------------------------------------------------------------------------- l
    /**
     * doParse method Read the Main Url and retrieve it's content
     */
    public void doParse(){
     HtmlReader reader=new HtmlReader();
     reader.configure(false);
        try {
            //page reader
            HtmlPage p=reader.readPage(MAIN_URL, 0);
            if(debug)
            System.out.println(p.asText());
            
       
        } catch (IOException ex) {
            Logger.getLogger(MBet365Parser.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    //-----------------------------------------------------------------------------------------------------
    public void setDebug(boolean debug) {
        this.debug = debug;
    }
    
}
