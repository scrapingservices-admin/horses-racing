/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package app.horseraces.betezy;

import app.horseraces.core.HtmlReader;
import app.horseraces.mbet365.MBet365Parser;
import com.gargoylesoftware.htmlunit.html.DomNode;
import com.gargoylesoftware.htmlunit.html.DomNodeList;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author DELL
 */
public class BetezyParser {

    private String MAIN_URL = "http://www.betezy.com.au/Races/";
    private String TOMORROW_URL = "http://www.betezy.com.au/Races/tomorrow";
    private boolean debug = false;
    HtmlReader reader = new HtmlReader();
    //----------------------------------------------------------------------------------------------- l

    /**
     * doParse method Read the Main Url and retrieve it's content
     */
    public void doParse() {

        reader.configure(false);
        try {
            //today reader
            HtmlPage p = reader.readPage(MAIN_URL, 0);
            HtmlPage p2 = reader.readPage(TOMORROW_URL, 0);
            if (debug) {
                System.out.println(p.asText());
            }
            parseToday(p);

        } catch (IOException ex) {
            Logger.getLogger(BetezyParser.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    //-----------------------------------------------------------------------------------------------------
    public void setDebug(boolean debug) {
        this.debug = debug;
    }

    private void parseToday(HtmlPage p) {
        DomNode f = p.querySelector("div[id=content] div[class=innernav] h2");
        String fecha = f.asText().split(" ")[1];
        System.out.println("Fecha: " + fecha);

        DomNodeList<DomNode> tr = p.querySelectorAll("table[id=ContentPlaceHolder1_RaceMatrix_tbGalloping] tr");

        for (int i = 1; i < tr.size(); i++) {
            String venue = tr.get(i).querySelector("td[class=meet_cell] span").asText();
            System.out.println("Venue: " + venue);
            DomNodeList<DomNode> td = tr.get(i).querySelectorAll("td");
            for (int j = 1; j < td.size(); j++) {
                String td_class = td.get(i).getAttributes().getNamedItem("class").getNodeValue();
                if (td_class.equals("race_cell")) {
                    System.out.println("Race");
                    System.out.println("Race time: " + td.get(j).asText());
                } else if (td_class.equals("race_cell resulted")) {
                    System.out.println("Race resulted");
                }
                if (td.get(j).hasChildNodes()) {
                    DomNode a = td.get(j).querySelector("a");
                    String link = a.getAttributes().getNamedItem("href").getNodeValue();
                    System.out.println("Race number: " + link.substring(link.lastIndexOf("/") + 1));
                    HtmlPage p2;
                    try {
                        p2 = reader.readPage("http://www.betezy.com.au" + link, 0);
                        parseRace(p2);
                    } catch (IOException ex) {
                        Logger.getLogger(BetezyParser.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    System.out.println("");
                }
            }
            System.out.println("----------------------------------------------");
        }
    }

    private void parseRace(HtmlPage p2) {
        // System.out.println(p2.asXml());
        DomNodeList<DomNode> horses = p2.querySelectorAll("div[class='selDetail ']");
        DomNodeList<DomNode> sc_horses = p2.querySelectorAll("div[class='selDetail selScratched']");

        System.out.println("Horses");
        System.out.println(horses.size());
        for (int i = 0; i < horses.size(); i++) {
            obtainData(horses.get(i));
        }

        System.out.println("Scratched Horses");
        System.out.println(sc_horses.size());
        for (int i = 0; i < sc_horses.size(); i++) {
            try {
                obtainData(sc_horses.get(i));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    private void obtainData(DomNode h) {
        String horseName = "-", horseNumber = "-", barrierNumber = "-", fixedPrice = "-";

        horseNumber = h.querySelector("div[class=selNo]").asText();
        horseName = h.querySelector("div[class=selName]").asText();
        try {
            barrierNumber = h.querySelector("div[class=selBarrier]").asText();
        } catch (Exception ex) {
        }
        try {
            fixedPrice = h.querySelector("div[class=selFixed]").asText();
        } catch (Exception ex) {
        }
        System.out.println("***" + horseNumber + " " + horseName + " " + barrierNumber + " " + fixedPrice);
    }
}
