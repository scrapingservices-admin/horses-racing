/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package app.horseraces.tattsbet;

import app.horseraces.unitbet.*;
import app.horseraces.core.HtmlReader;
import app.horseraces.mbet365.MBet365Parser;
import com.gargoylesoftware.htmlunit.html.DomNode;
import com.gargoylesoftware.htmlunit.html.DomNodeList;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author DELL This class needs to perform click to get today/tomorrow races
 * table and a specific
 */
public class TattsbetParser {

    private String MAIN_URL = "https://tatts.com/racing";
    private String TOMORROW_URL = "";
    private boolean debug = false;
    HtmlReader reader = new HtmlReader();
    //----------------------------------------------------------------------------------------------- l

    /**
     * doParse method Read the Main Url and retrieve it's content
     */
    public void doParse() {

        reader.configure(false);
        try {
            //page reader
            HtmlPage p = reader.readPage(MAIN_URL, 0);
            if (debug) {
                System.out.println(p.asText());
            }

//            TOMORROW_URL = "https://tatts.com" + p.querySelector("li[id=RD140909-ITEM] a").getAttributes().getNamedItem("href").getNodeValue();

            parseToday(p);
        } catch (IOException ex) {
            Logger.getLogger(TattsbetParser.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    //-----------------------------------------------------------------------------------------------------
    public void setDebug(boolean debug) {
        this.debug = debug;
    }

    private void parseToday(HtmlPage p) {
        String fulldate = p.querySelector("span[id=raceDayLabel]").asText();
        System.out.println(fulldate);
        /*String day = fulldate.split(", ")[0];
         String date = fulldate.split(", ")[1];
         System.out.println(day + " " + date);
         */
        DomNodeList<DomNode> tr = p.querySelectorAll("table[id=page_R1] tr[class=showhide_R]");
        for (int i = 0; i < tr.size(); i++) {
            DomNodeList<DomNode> td = tr.get(i).querySelectorAll("td");
            String venue = td.get(1).asText();
            System.out.println("Race Venue: " + venue);

            for (int j = 0; j < td.size(); j++) {
                String link = "";
                try {
                    DomNode a = td.get(j).querySelector("a[class=grid-race-details-text]");
                    link = a.getAttributes().getNamedItem("href").getNodeValue();
//                    if (td.get(j).getAttributes().getNamedItem("class").getNodeValue().equals("grid-white")) {
//                        System.out.println("Race time: " + a.asText());
//                    }
                } catch (Exception e) {
                }
                if (!link.equals("")) {
                    String racenumber = link.substring(link.lastIndexOf("/") + 1);
                    System.out.println("Race Number: " + racenumber);

                    try {
                        HtmlPage p2 = reader.readPage("https://tatts.com" + link, 0);
                        parseRace(p2);
                    } catch (IOException ex) {
                        Logger.getLogger(TattsbetParser.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
            }
            System.out.println("--------------------------------------------------------------");
        }
    }

    private void parseRace(HtmlPage p2) {

        DomNode content = p2.querySelector("div[id=contentPane]");

        String time = content.querySelector("span[class=CountdownTimer]").asText();
        System.out.println("Race Time: " + time);
        DomNode table = content.querySelector("div[style='display:block;overflow:hidden;'] table td[bgcolor='#e6e6e6'] table tbody");

        DomNodeList<DomNode> odd_tr = table.querySelectorAll("tr[bgcolor='#eaeaea']");
        DomNodeList<DomNode> even_tr = table.querySelectorAll("tr[bgcolor='#f7f7f7']");
        DomNodeList<DomNode> scratched_tr = table.querySelectorAll("tr[class=Scratched-Runner]");

        DomNodeList<DomNode> grey = content.querySelectorAll("td[class=subtitle-bg-grey]");
        String date, distance, weather, track;
        date = grey.get(0).asText().split("\n")[1].trim();
        distance = grey.get(1).asText().split("\n")[1].trim();
        weather = grey.get(2).asText().split("\n")[1].trim();
        track = grey.get(3).asText().split("\n")[1].trim();

        System.out.println(date + " " + distance + " " + weather + " " + track);

        for (int i = 0; i < scratched_tr.size(); i++) {
            obtainData(scratched_tr.get(i));
        }
    }

    private void obtainData(DomNode a) {
        DomNodeList<DomNode> td = a.querySelectorAll("td[height='27']");
        String number = td.get(1).asText();
        String name = td.get(2).asText();
        String fixedwin = "-", fixedplace = "-";
        try {
            DomNode tab = null;
            DomNodeList<DomNode> tabs = a.querySelectorAll("td");
            for (int i = 0; i < tabs.size(); i++) {
                try {
                    if (tabs.get(i).getAttributes().getNamedItem("id").getNodeValue().contains("_fixedOdds")) {
                        tab = tabs.get(i);
                    }
                    fixedwin = tab.querySelectorAll("td").get(1).asText().trim();                  
                    fixedplace = tab.querySelectorAll("td").get(2).asText().trim();
                } catch (Exception e) {
                }
            }           
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("***" + number + " " + name + " " + fixedwin + " " + fixedplace);


    }
}
