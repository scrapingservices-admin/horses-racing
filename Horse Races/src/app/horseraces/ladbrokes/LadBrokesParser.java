/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package app.horseraces.ladbrokes;

import app.horseraces.core.HorseData;
import app.horseraces.core.HorseUtils;
import app.horseraces.core.HtmlReader;
import app.horseraces.mbet365.MBet365Parser;
import com.gargoylesoftware.htmlunit.html.DomNode;
import com.gargoylesoftware.htmlunit.html.DomNodeList;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author DELL
 * Races and Tomorrow Races on the main Page
 */
public class LadBrokesParser {
     private String MAIN_URL="https://www.ladbrokes.com.au/racing/horses/";
    
    
    private boolean debug=false;
    
   //----------------------------------------------------------------------------------------------- l
    /**
     * doParse method Read the Main Url and retrieve it's content
     */
    public void doParse(){
     HtmlReader reader=new HtmlReader();
     reader.configure(false);
        try {
            //page reader
            HtmlPage p=reader.readPage(MAIN_URL, 0);
            
            if(debug)
            System.out.println(p.asText());
            
          List<HorseData> data=  parseRacesHome(p);
       
            HorseUtils.printRaces(data);
        } catch (IOException ex) {
            Logger.getLogger(LadBrokesParser.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }//end do Parse
    
    /*Parse the horses at home page */
   //--------------------------------------------------------------------------- 
    public List<HorseData> parseRacesHome(HtmlPage p){
      
        List<HorseData> data=new ArrayList<HorseData>();
      DomNodeList<DomNode> tr=p.querySelector("table[class='listings horses bettype-group']").querySelectorAll("tr[class='row meeting'] td[class='racing-location']");
      HorseData hd=null;
      
        for (int i = 0; i < tr.size(); i++) {
            hd=new HorseData();
            hd.racingVenues=tr.get(i).asText();
            
            data.add(hd);
        }
  
      return data;
        
    }

    //-----------------------------------------------------------------------------------------------------
    public void setDebug(boolean debug) {
        this.debug = debug;
    }
}
